import { Entity, PrimaryGeneratedColumn, Column } from "typeorm"

@Entity()
export class Product {

    @PrimaryGeneratedColumn({name: "produc_id"})
    id: number

    @Column({name: "produc_name"})
    name: string

    @Column({name: "produc_price"})
    price: number

}
